import Vue from 'vue'
import Router from 'vue-router'
import catalog from '@/components/catalog'
import children from '@/components/children'
import basket from '@/components/basket'
import product from '@/components/product'
import products from '@/components/products'
import detailssmeta from '@/components/detailssmeta'
import smeta from '@/components/smeta'


Vue.use(Router)
//dev
const domainPath="http://opt.loc";
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'сatalog',
      component: catalog
    },
    {
      path: '/catalog/:uid',
      name: 'children',
      component: children,
      props: (route) => ({load_uri: domainPath+'/api/products', category_uid: route.params.uid, f: route.query.f || '', pag : route.query.page || 1, per_pag : route.query.per_page || 10, sorts: route.query.sort || 'title_product|asc' })
    },
    {
      path: '/basket',
      name: 'basket',
      component: basket
    },
    {
      path: '/catalog/:uid/:parentuid',
      name: 'products',
      component: products
    },
    {
      path: '/product/:uid',
      name: 'product',
      component: product
    },
    {
      path: '/estimate',
      name: 'estimate',
      components: smeta
    },
    {
      path: '/estimate/:uid',
      name: 'estimatedateils',
      component: detailssmeta
    }
  ]
})
